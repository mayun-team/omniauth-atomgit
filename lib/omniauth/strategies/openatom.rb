require 'omniauth-oauth2'

module OmniAuth
  module Strategies
    class Openatom < OmniAuth::Strategies::OAuth2
      option :name, 'openatom'

      option :client_options, {
        site: 'https://api.atomgit.com',
        authorize_url: 'https://atomgit.com/login/oauth/authorize',
        token_url: 'https://api.atomgit.com/login/oauth/access_token'
      }

      def request_phase
        super
      end

      uid { raw_info['id'].to_s }

      info do
        {
          image: raw_info['avatar_url'],
          name: raw_info['name'],
          email: raw_info['email'],
          nickname: raw_info['name']
        }
      end

      extra do
        {
          'raw_info' => raw_info
        }
      end

      def raw_info
        @raw_info ||= access_token.get('/user/info').parsed
      end
    end
  end
end

OmniAuth.config.add_camelization 'openatom', 'Openatom'