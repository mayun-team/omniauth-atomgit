# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'omniauth-openatom/version'

Gem::Specification.new do |spec|
  spec.name          = 'omniauth-openatom'
  spec.version       = Omniauth::Openatom::VERSION
  spec.authors       = ['GZK']
  spec.email         = ['2784548288@qq.com']
  spec.description   = %q{Oauth2 Gem for atomgit.com}
  spec.summary       = %q{Oauth2 Gem for atomgit.com}
  spec.homepage      = 'https://gitee.com/mayun-team/omniauth-atomgit'
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
